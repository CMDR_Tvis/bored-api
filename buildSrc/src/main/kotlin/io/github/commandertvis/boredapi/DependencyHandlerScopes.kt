package io.github.commandertvis.boredapi

import org.gradle.kotlin.dsl.DependencyHandlerScope

public fun DependencyHandlerScope.jacksonCore(module: String, version: String? = null): Any = buildString {
    append("com.fasterxml.jackson.core:jackson-")
    append(module)

    if (version != null) {
        append(':')
        append(version)
    }
}

public fun DependencyHandlerScope.kohttp(module: String? = null, version: String? = null): Any = buildString {
    append("io.github.rybalkinsd:kohttp")

    if (module != null) {
        append('-')
        append(module)
    }

    if (version != null) {
        append(':')
        append(version)
    }
}

public fun DependencyHandlerScope.kotlinxCoroutines(module: String, version: String? = null): Any = buildString {
    append("org.jetbrains.kotlinx:kotlinx-coroutines-")
    append(module)

    if (version != null) {
        append(':')
        append(version)
    }
}

public fun DependencyHandlerScope.okhttp3(module: String, version: String? = null): Any = buildString {
    append("com.squareup.okhttp3:")
    append(module)

    if (version != null) {
        append(':')
        append(version)
    }
}

public fun DependencyHandlerScope.slf4j(module: String, version: String? = null): Any = buildString {
    append("org.slf4j:slf4j-")
    append(module)

    if (version != null) {
        append(':')
        append(version)
    }
}
