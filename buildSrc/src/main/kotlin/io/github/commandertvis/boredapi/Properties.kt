package io.github.commandertvis.boredapi

import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.SetProperty
import java.net.URL

public operator fun <T> Property<T>.invoke(value: T?): Unit = set(value)
public operator fun Property<URL?>.invoke(spec: String?): Unit = set(spec?.let(::URL))
public operator fun <T> SetProperty<T>.invoke(vararg elements: T): Unit = set(hashSetOf(*elements))
public operator fun <T> ListProperty<T>.invoke(vararg elements: T): Unit = set(mutableListOf(*elements))
public operator fun <K, V> MapProperty<K, V>.invoke(vararg entries: Pair<K, V>): Unit = set(hashMapOf(*entries))
