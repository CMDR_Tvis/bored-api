package io.github.commandertvis.boredapi

import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.kotlin.dsl.maven

public fun RepositoryHandler.boredApiGitlab(action: MavenArtifactRepository.() -> Unit): MavenArtifactRepository =
    maven(url = "https://gitlab.com/api/v4/projects/16347919/packages/maven", action = action)
