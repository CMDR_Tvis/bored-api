package io.github.commandertvis.boredapi

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.net.URL
import kotlin.test.assertEquals

internal class BoredApiTestKotlin {
    @Test
    fun randomAsync() = TestHelper.useApiWithMockServer(
        arrayOf(
            "{\"activity\":\"Read a formal research paper on an interesting subject\",\"accessibility\":0.1,\"type\":\"recreational\",\"participants\":1,\"price\":0,\"link\":\"\",\"key\":\"3352474\"}",
            "{\"activity\":\"Learn about the Golden Ratio\",\"accessibility\":0.2,\"type\":\"education\",\"participants\":1,\"price\":0.1,\"link\":\"https://en.wikipedia.org/wiki/Golden_ratio\",\"key\":\"2095681\"}"
        )
    ) { api ->
        assertEquals(
            Activity(
                description = "Read a formal research paper on an interesting subject",
                accessibility = 0.1,
                type = ActivityType.RECREATIONAL,
                participants = 1,
                price = 0.0,
                link = null,
                key = 3352474
            ),

            runBlocking { api.randomAsync().await() }
        )

        assertEquals(
            Activity(
                description = "Learn about the Golden Ratio",
                accessibility = 0.2,
                type = ActivityType.EDUCATION,
                participants = 1,
                price = 0.1,
                link = URL("https://en.wikipedia.org/wiki/Golden_ratio"),
                key = 2095681
            ),

            runBlocking { api.randomAsync().await() }
        )
    }

    @Test
    fun random() = TestHelper.useApiWithMockServer(
        arrayOf(
            "{\"activity\":\"Read a formal research paper on an interesting subject\",\"accessibility\":0.1,\"type\":\"recreational\",\"participants\":1,\"price\":0,\"link\":\"\",\"key\":\"3352474\"}",
            "{\"activity\":\"Learn about the Golden Ratio\",\"accessibility\":0.2,\"type\":\"education\",\"participants\":1,\"price\":0.1,\"link\":\"https://en.wikipedia.org/wiki/Golden_ratio\",\"key\":\"2095681\"}"
        )
    ) { api ->
        assertEquals(
            Activity(
                description = "Read a formal research paper on an interesting subject",
                accessibility = 0.1,
                type = ActivityType.RECREATIONAL,
                participants = 1,
                price = 0.0,
                link = null,
                key = 3352474
            ),

            api.random()
        )

        assertEquals(
            Activity(
                description = "Learn about the Golden Ratio",
                accessibility = 0.2,
                type = ActivityType.EDUCATION,
                participants = 1,
                price = 0.1,
                link = URL("https://en.wikipedia.org/wiki/Golden_ratio"),
                key = 2095681
            ),

            api.random()
        )
    }

    @Test
    fun byCriteriaAsync() = TestHelper.useApiWithMockServer(
        arrayOf("{\"activity\":\"Listen to a new podcast\",\"accessibility\":0.12,\"type\":\"relaxation\",\"participants\":1,\"price\":0.05,\"link\":\"\",\"key\":\"4124860\"}")
    ) { api ->
        assertEquals(
            Activity(
                description = "Listen to a new podcast",
                accessibility = 0.12,
                type = ActivityType.RELAXATION,
                participants = 1,
                price = 0.05,
                link = null,
                key = 4124860
            ),

            runBlocking {
                api.byCriteriaAsync {
                    ExactAccessibility set 0.12
                    MinimalAccessibility set 0.12
                    MaximalAccessibility set 0.12
                    Type set ActivityType.RELAXATION
                    Participants set 1
                    ExactPrice set 0.05
                    MinimalPrice set 0.05
                    MaximalPrice set 0.05
                    Key set 4124860
                }.await()
            }
        )
    }

    @Test
    fun byCriteria() = TestHelper.useApiWithMockServer(
        arrayOf("{\"activity\":\"Listen to a new podcast\",\"accessibility\":0.12,\"type\":\"relaxation\",\"participants\":1,\"price\":0.05,\"link\":\"\",\"key\":\"4124860\"}")
    ) { api ->
        assertEquals(
            Activity(
                description = "Listen to a new podcast",
                accessibility = 0.12,
                type = ActivityType.RELAXATION,
                participants = 1,
                price = 0.05,
                link = null,
                key = 4124860
            ),

            api.byCriteria {
                ExactAccessibility set 0.12
                MinimalAccessibility set 0.12
                MaximalAccessibility set 0.12
                Type set ActivityType.RELAXATION
                Participants set 1
                ExactPrice set 0.05
                MinimalPrice set 0.05
                MaximalPrice set 0.05
                Key set 4124860
            }
        )
    }
}
