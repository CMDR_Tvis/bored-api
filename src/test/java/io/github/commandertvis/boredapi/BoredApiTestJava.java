package io.github.commandertvis.boredapi;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

public final class BoredApiTestJava {
    @Test
    public final void randomAsync() {
        TestHelper.useApiWithMockServer(
                new String[]{"{\"activity\":\"Read a formal research paper on an interesting subject\",\"accessibility\":0.1,\"type\":\"recreational\",\"participants\":1,\"price\":0,\"link\":\"\",\"key\":\"3352474\"}",
                        "{\"activity\":\"Learn about the Golden Ratio\",\"accessibility\":0.2,\"type\":\"education\",\"participants\":1,\"price\":0.1,\"link\":\"https://en.wikipedia.org/wiki/Golden_ratio\",\"key\":\"2095681\"}"},

                api -> {
                    try {
                        Assertions.assertEquals(new Activity(
                                "Read a formal research paper on an interesting subject",
                                0.1,
                                ActivityType.RECREATIONAL,
                                1,
                                0.0,
                                null,
                                3352474
                        ), api.randomAsync().get());
                    } catch (@NotNull final InterruptedException | ExecutionException e) {
                        Assertions.fail(e);
                    }
                });
    }

    @Test
    public final void byCriteriaAsync() {
        TestHelper.useApiWithMockServer(
                new String[]{"{\"activity\":\"Listen to a new podcast\",\"accessibility\":0.12,\"type\":\"relaxation\",\"participants\":1,\"price\":0.05,\"link\":\"\",\"key\":\"4124860\"}"},

                api -> {
                    try {
                        Assertions.assertEquals(new Activity(
                                        "Listen to a new podcast",
                                        0.12,
                                        ActivityType.RELAXATION,
                                        1,
                                        0.05,
                                        null,
                                        4124860
                                ),

                                api.byCriteriaAsync(criterionSelection -> {
                                    criterionSelection.set(Criteria.getExactAccessibility(), 0.12);
                                    criterionSelection.set(Criteria.getMinimalAccessibility(), 0.12);
                                    criterionSelection.set(Criteria.getMaximalAccessibility(), 0.12);
                                    criterionSelection.set(Criteria.getType(), ActivityType.RELAXATION);
                                    criterionSelection.set(Criteria.getParticipants(), 1);
                                    criterionSelection.set(Criteria.getExactPrice(), 0.05);
                                    criterionSelection.set(Criteria.getMinimalPrice(), 0.05);
                                    criterionSelection.set(Criteria.getMaximalPrice(), 0.05);
                                    criterionSelection.set(Criteria.getKey(), 4124860);
                                }).get());
                    } catch (@NotNull final InterruptedException | ExecutionException e) {
                        Assertions.fail(e);
                    }
                });
    }
}
