package io.github.commandertvis.boredapi;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.platform.commons.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.function.Consumer;

public final class TestHelper {
    private static final Logger logger = LoggerFactory.getLogger(TestHelper.class);

    private TestHelper(){
    }


    public static void useApiWithMockServer(@NotNull final String[] jsonResponsesQueue,
                                            @NotNull final Consumer<BoredApi> action) {
        Preconditions.notNull(jsonResponsesQueue, "jsonResponsesQueue");
        Preconditions.notNull(action, "action");

        try (@NotNull final MockWebServer server = new MockWebServer()) {
            @NotNull final BoredApi api = new BoredApi(server.url("/api/activity").url());
            logger.info("Initializing the API on the endpoint {}.", api.getEndpoint());

            Arrays.stream(jsonResponsesQueue)
                    .map(responseBody -> new MockResponse().addHeader("Content-Type", "application/json; charset=utf-8")
                            .setBody(responseBody)
                            .setResponseCode(HttpURLConnection.HTTP_OK))

                    .forEach(server::enqueue);

            action.accept(api);
        } catch (@NotNull final IOException e) {
            Assertions.fail(e);
        }
    }
}

