package io.github.commandertvis.boredapi

import java.io.Serializable
import java.net.URL

/**
 * Represents Activity entity of Bored API.
 *
 * @property description The description of activity.
 * @property accessibility A factor describing how possible an event is to do with zero being the most accessible.
 * @property type The type of the activity.
 * @property participants The number of people that this activity could involve.
 * @property price A factor describing the cost of the event with zero being free.
 * @property key A unique numeric ID.
 */
public data class Activity public constructor(
    public val description: String,
    public val accessibility: Double,
    public val type: ActivityType,
    public val participants: Int,
    public val price: Double,
    public val link: URL?,
    public val key: Int
) : Serializable {
    init {
        require(accessibility in 0.0..1.0) { "Accessibility $accessibility is out of range: 0.0..1.0." }
        require(participants >= 1) { "Quantity of participants $participants cannot be less than 1." }
        require(price in 0.0..1.0) { "Price $price is out of range: 0.0..1.0." }
        require(key in 1_000_000..9_999_999) {"Key $key is out range: 1 000 000..9 999 999"}
    }

    private companion object {
        private const val serialVersionUID = 2307505517490940483L
    }
}
