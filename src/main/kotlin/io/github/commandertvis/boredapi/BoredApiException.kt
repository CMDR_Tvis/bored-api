package io.github.commandertvis.boredapi

import java.io.Serializable

/**
 * Thrown, when Bored API returns error response.
 *
 * @param message the error message, provided in the response.
 */
public class BoredApiException internal constructor(message: String?) : RuntimeException(message), Serializable {
    private companion object {
        private const val serialVersionUID = 3316637789890257777L
    }
}
