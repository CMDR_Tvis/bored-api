package io.github.commandertvis.boredapi

/**
 * Represents a type of activity in Bored API.
 *
 * @see Activity
 */
public enum class ActivityType {
    EDUCATION, RECREATIONAL, SOCIAL, DIY, CHARITY, COOKING, RELAXATION, MUSIC, BUSYWORK;

    public override fun toString(): String = name.toLowerCase()
}
