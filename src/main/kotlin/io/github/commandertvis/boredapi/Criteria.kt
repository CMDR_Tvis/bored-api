@file:JvmName("Criteria")

package io.github.commandertvis.boredapi

/**
 * According to this criterion, an [Activity], which matches it, should have a certain accessibility.
 */
public val ExactAccessibility: DoubleRangedCriterion =
    DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "accessibility")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain price.
 */
public val ExactPrice: DoubleRangedCriterion = DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "price")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain key.
 */
public val Key: IntRangedCriterion = IntRangedCriterion(possibleValues = 1_000_000..9_999_999, name = "key")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain accessibility or lower one.
 */
public val MaximalAccessibility: DoubleRangedCriterion =
    DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "maxaccessibility")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain price or lower one.
 */
public val MaximalPrice: DoubleRangedCriterion = DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "maxprice")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain accessibility or higher one.
 */
public val MinimalAccessibility: DoubleRangedCriterion =
    DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "minaccessibility")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain price or higher one.
 */
public val MinimalPrice: DoubleRangedCriterion = DoubleRangedCriterion(possibleValues = 0.0..1.0, name = "minprice")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain quantity of participants.
 */
public val Participants: IntRangedCriterion =
    IntRangedCriterion(possibleValues = 0..Int.MAX_VALUE, name = "participants")

/**
 * According to this criterion, an [Activity], which matches it, should have a certain type.
 *
 * @see ActivityType
 */
public val Type: ActivityCriterion<ActivityType> = ActivityCriterion("type")

/**
 * Represents a criterion to select activities by. It also encapsulates validating of values, providable to this
 * criterion.
 *
 * @param T the type of values of the criterion.
 * @property name The name of criterion. It coincides with the GET parameter of the API.
 */
public open class ActivityCriterion<T> internal constructor(public val name: String) {
    @JvmSynthetic
    internal open fun validateValue(value: T) = Unit
}

/**
 * Represents a criterion, expressed by a comparable value. All the values provided to this criterion must be in
 * possibleValues range.
 *
 * @param T the type of values of the criterion.
 * @param possibleValues the range of possible values.
 * @param name the name of criterion.
 */
public sealed class RangedCriterion<T : Comparable<T>>(private val possibleValues: ClosedRange<T>, name: String) :
    ActivityCriterion<T>(name) {
    @JvmSynthetic
    internal final override fun validateValue(value: T) =
        require(value in possibleValues) { "Value $value is out of range: $possibleValues." }
}

/**
 * Represents a criterion, expressed by a [Double]. All the values provided to this criterion must be in
 * possibleValues range.
 *
 * @param possibleValues the range of possible values.
 * @param name the name of criterion.
 */
public class DoubleRangedCriterion internal constructor(
    possibleValues: ClosedRange<Double>,
    name: String
) : RangedCriterion<Double>(possibleValues, name)

/**
 * Represents a criterion, expressed by an [Int]. All the values provided to this criterion must be in
 * possibleValues range.
 *
 * @param possibleValues the range of possible values.
 * @param name the name of criterion.
 */
public class IntRangedCriterion internal constructor(
    possibleValues: ClosedRange<Int>,
    name: String
) : RangedCriterion<Int>(possibleValues, name)
