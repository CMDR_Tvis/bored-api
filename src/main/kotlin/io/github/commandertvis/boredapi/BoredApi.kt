package io.github.commandertvis.boredapi

import com.fasterxml.jackson.databind.JsonNode
import io.github.rybalkinsd.kohttp.client.defaultHttpClient
import io.github.rybalkinsd.kohttp.client.fork
import io.github.rybalkinsd.kohttp.dsl.async.httpGetAsync
import io.github.rybalkinsd.kohttp.dsl.context.ParamContext
import io.github.rybalkinsd.kohttp.dsl.httpGet
import io.github.rybalkinsd.kohttp.ext.url
import io.github.rybalkinsd.kohttp.interceptors.logging.HttpLoggingInterceptor
import io.github.rybalkinsd.kohttp.jackson.ext.toJson
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.future.asCompletableFuture
import okhttp3.OkHttpClient
import org.slf4j.LoggerFactory
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.CompletableFuture
import java.util.function.Consumer

/**
 * The Bored API Kotlin implementation.
 *
 * @property endpoint The endpoint of Activity API.
 * @property client The [OkHttpClient], used by the API.
 */
public class BoredApi @JvmOverloads public constructor(
    public val endpoint: URL = URL("http://www.boredapi.com/api/activity"),
    public val client: OkHttpClient = defaultHttpClient
) {
    init {
        client.fork { interceptors { +HttpLoggingInterceptor { logger.debug(it) } } }
    }

    /**
     * Requests a random Activity from the API asynchronously.
     *
     * @return a [Deferred] of activity.
     */
    @JvmName("randomAsync$1")
    @JvmSynthetic
    public fun randomAsync(): Deferred<Activity> = requestActivityAsync()

    /**
     * Requests a random Activity from the API asynchronously.
     *
     * @return a [CompletableFuture] of activity.
     */
    @JvmName("randomAsync")
    public fun randomAsyncJava(): CompletableFuture<Activity> = randomAsync().asCompletableFuture()

    /**
     * Requests a random Activity from the API.
     *
     * @return either a random activity or `null` in case of problems with API.
     */
    public fun random(): Activity = requestActivity()

    /**
     * Requests a random activity from the API asynchronously. Either the resulting activity will fulfill the
     * conditions of provided [selection], or an exception will be thrown.
     *
     * @param selection configuring function to [CriteriaSelection] object. Java method takes [Consumer] instead of
     * Kotlin's [Function].
     *
     * @return a [Deferred] of suitable activity.
     */
    @JvmName("byCriteriaAsync$1")
    @JvmSynthetic
    public fun byCriteriaAsync(selection: CriteriaSelection.() -> Unit): Deferred<Activity> = requestActivityAsync {
        CriteriaSelection().apply(selection).parameters.forEach { (k, v) -> k to v }
    }

    /**
     * Requests a random activity from the API asynchronously. Either the resulting activity will fulfill the
     * conditions of provided [selection], or an exception will be thrown.
     *
     * @param selection configuring function to [CriteriaSelection] object. Java method takes [Consumer] instead of
     * Kotlin's [Function].
     *
     * @return a [CompletableFuture] of suitable activity.
     */
    @JvmName("byCriteriaAsync")
    public fun byCriteriaAsyncJava(selection: Consumer<CriteriaSelection>): CompletableFuture<Activity> =
        byCriteriaAsync { selection.accept(this) }.asCompletableFuture()

    /**
     * Requests a random activity from the API. Either the resulting activity will fulfill the conditions of provided
     * [selection], or an exception will be thrown.
     *
     * @param selection configuring function to [CriteriaSelection] object. Java method takes [Consumer] instead of
     * Kotlin's [Function].
     *
     * @return a suitable activity.
     */
    @JvmName("byCriteria$1")
    @JvmSynthetic
    public fun byCriteria(selection: CriteriaSelection.() -> Unit): Activity =
        requestActivity { CriteriaSelection().apply(selection).parameters.forEach { (k, v) -> k to v } }

    /**
     * Requests a random activity from the API. Either the resulting activity will fulfill the conditions of provided
     * [selection], or an exception will be thrown.
     *
     * @param selection configuring function to [CriteriaSelection] object. Java method takes [Consumer] instead of
     * Kotlin's [Function].
     *
     * @return a suitable activity.
     */
    @JvmName("byCriteria")
    public fun byCriteriaJava(selection: Consumer<CriteriaSelection>): Activity =
        requestActivity { byCriteria { selection.accept(this) } }

    private fun requestActivityAsync(params: ParamContext.() -> Unit = {}): Deferred<Activity> =
        GlobalScope.async(Dispatchers.Unconfined) {
            httpGetAsync(client) { url(endpoint); param(params) }
                .await()
                .use { deserialize(it.toJson()) }
        }

    private fun requestActivity(params: ParamContext.() -> Unit = {}): Activity =
        httpGet(client) { url(endpoint); param(params) }.use { deserialize(it.toJson()) }

    private fun deserialize(json: JsonNode): Activity {
        logger.trace("JSON: {}.", json)

        if (json["error"] != null)
            throw BoredApiException(json["error"]?.asText())

        val description = json["activity"]!!.asText()
        val accessibility = json["accessibility"]!!.asDouble()
        val type = ActivityType.valueOf(json["type"]!!.asText()!!.toUpperCase())
        val participants = json["participants"]!!.asInt()
        val price = json["price"]!!.asDouble()
        val linkNode = json["link"]!!

        val link = try {
            URL(linkNode.asText())
        } catch (ignored: MalformedURLException) {
            null
        }

        val key = json["key"]!!.asInt()
        val activity = Activity(description, accessibility, type, participants, price, link, key)
        logger.trace("New Activity: }", activity)
        return activity
    }

    private companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)!!
    }

    /**
     * Stores a selection of several criteria to activities requested.
     */
    public class CriteriaSelection internal constructor() {
        @JvmField
        @JvmSynthetic
        internal val parameters: MutableMap<String, String> = hashMapOf()

        /**
         * Sets a certain value to provided criterion.
         *
         * @param T the type of criterion's value.
         * @receiver the criterion to update.
         * @param value the value to set.
         */
        public infix fun <T> ActivityCriterion<T>.set(value: T) {
            validateValue(value)
            parameters[name] = value.toString()
        }
    }
}
