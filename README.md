# bored-api

A [Bored API](https://www.boredapi.com) implementation in Kotlin. 

## Including

**build.gradle.kts**

```kotlin
repositories {
    maven(url = "https://gitlab.com/api/v4/projects/16347919/packages/maven")
}

dependencies {
    implementation("io.github.commandertvis:bored-api:2.2.0")
}
```

**build.gradle**

```gradle
repositories {
    maven { url 'https://gitlab.com/api/v4/projects/16347919/packages/maven' }
}

dependencies {
    implementation 'io.github.commandertvis:bored-api:2.2.0'
}
```

**pom.xml**

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/16347919/packages/maven</url>
  </repository>
</repositories>

<dependency>
  <groupId>io.github.commandertvis</groupId>
  <artifactId>bored-api</artifactId>
  <version>2.2.0</version>
</dependency>
```

**bored-api-2.2.0.jar**

https://gitlab.com/CMDR_Tvis/bored-api/-/jobs/artifacts/master/raw/build/libs/bored-api-2.2.0.jar?job=publish

## Examples

**Write a random activity:**

```kotlin
import io.github.commandertvis.boredapi.BoredApi

fun main() {
    val activity = BoredApi().random()
    println(activity)
}
```

```java
import io.github.commandertvis.boredapi.Activity;
import io.github.commandertvis.boredapi.BoredApi;

public final class Example {
    private Example() {
    }

    public static void main(String[] args) {
        Activity activity = new BoredApi().random();
        System.out.println(activity);
    }
}
```

**Write a random activity with price bound:**

```kotlin
import io.github.commandertvis.boredapi.BoredApi
import io.github.commandertvis.boredapi.MaximalPrice
import io.github.commandertvis.boredapi.MinimalPrice

fun main() {
    val activity = BoredApi().byCriteria { 
        MinimalPrice set 0.25
        MaximalPrice set 0.5
    }
        
    println(activity)
}
```

```java
import io.github.commandertvis.boredapi.Activity;
import io.github.commandertvis.boredapi.BoredApi;
import io.github.commandertvis.boredapi.Criteria;

public final class Example {
    private Example() {
    }

    public static void main(String[] args) {
        Activity activity = new BoredApi().byCriteria(selection -> {
            selection.set(Criteria.getMinimalPrice(), 0.25);
            selection.set(Criteria.getMaximalPrice(), 0.5);
        });
        
        System.out.println(activity);
    }
}
```

**Write an activity's description by its key asynchronously:**

```kotlin
import io.github.commandertvis.boredapi.BoredApi
import io.github.commandertvis.boredapi.Key
import kotlinx.coroutines.runBlocking

fun main() {
    val activity = runBlocking { BoredApi().byCriteriaAsync { Key set 8979931 }.await() }
    println(activity.description)
}
```

```java
import io.github.commandertvis.boredapi.Activity;
import io.github.commandertvis.boredapi.BoredApi;
import io.github.commandertvis.boredapi.Criteria;

import java.util.concurrent.ExecutionException;

public final class Example {
    private Example() {
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Activity activity = new BoredApi()
                .byCriteriaAsync(selection -> selection.set(Criteria.getKey(), 8979931))
                .get();
        
        System.out.println(activity.getDescription());
    }
}
```

## Logging

This project uses SLF4J for logging. You need to have its [implementation](http://www.slf4j.org/manual.html#swapping) in classpath of your application. 

## Java Interoperability

The library is completely Java-interoperable, but the API looks a bit different in Java and Kotlin, because Deferred objects are converted to CompletableFuture, and Kotlin functions to Java Consumer lambdas. The API is tested both in Java and Kotlin.

## Documentation

The project's Dokka generated documentation is published [here](http://cmdr_tvis.gitlab.io/bored-api/bored-api). 

## Licensing

This project is licensed under the [MIT license](LICENSE).
