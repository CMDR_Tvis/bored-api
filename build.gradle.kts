import io.github.commandertvis.boredapi.*
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    `maven-publish`
    id("org.jetbrains.dokka")
    kotlin(module = "jvm")
}

val boredApiVersion: String by project
val jacksonVersion: String by project
val junitJupiterVersion: String by project
val kohttpVersion: String by project
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val kotlinxCoroutinesVersion: String by project
val mockWebServerVersion: String by project
val okhttpVersion: String by project
val okioVersion: String by project
val slf4JVersion: String by project
group = "io.github.commandertvis"
version = boredApiVersion
repositories.jcenter()

dependencies {
    api("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    api("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
    api("com.squareup.okio:okio:$okioVersion")
    api(kotlinxCoroutines(module = "core", version = kotlinxCoroutinesVersion))
    api(kotlinxCoroutines(module = "jdk8", version = kotlinxCoroutinesVersion))
    api(jacksonCore(module = "core", version = jacksonVersion))
    api(kohttp(module = "jackson", version = kohttpVersion))
    api(kohttp(version = kohttpVersion))
    api(okhttp3(module = "okhttp", version = okhttpVersion))
    api(slf4j("api", slf4JVersion))
    implementation(kotlin(module = "reflect", version = kotlinVersion))
    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation(okhttp3(module = "mockwebserver", version = mockWebServerVersion))
    testImplementation(slf4j("jdk14", slf4JVersion))
    testImplementation(kotlin(module = "test", version = kotlinVersion))
    testImplementation(kotlin(module = "test-junit5", version = kotlinVersion))
}

tasks {
    dokkaHtml.get().outputDirectory(file("public/"))

    withType<KotlinCompile> {
        kotlinOptions {
            apiVersion = kotlinApiVersion
            jvmTarget = kotlinJvmTarget
            languageVersion = kotlinLanguageVersion
        }
    }

    publish.get().dependsOn(build)
    test.get().useJUnitPlatform()

    val sourcesJar by creating(Jar::class) {
        from(sourceSets["main"].allSource)
        archiveClassifier.set("sources")
    }

    val dokkaJar by creating(Jar::class) {
        dependsOn(dokkaJavadoc)
        group = JavaBasePlugin.DOCUMENTATION_GROUP
        archiveClassifier("javadoc")
        from(javadoc.get().destinationDir)
    }

    withType<DokkaTask> {
        dokkaSourceSets.configureEach {
            includes.setFrom(includes + "PACKAGES.md")
            jdkVersion(8)

            externalDocumentationLink {
                url("https://javadoc.io/doc/com.squareup.okhttp3/okhttp/$okhttpVersion/")
                packageListUrl("https://javadoc.io/doc/com.squareup.okhttp3/okhttp/$okhttpVersion/package-list")
            }

            perPackageOption { prefix("kotlin") }

            sourceLink {
                localDirectory(file("src/main/kotlin"))
                remoteUrl("https://gitlab.com/CMDR_Tvis/${project.name}/tree/master/src/main/kotlin/")
                remoteLineSuffix("#L")
            }
        }
    }

    publishing {
        publications {
            val mavenJava by creating(MavenPublication::class) {
                from(project.components["java"])

                artifacts {
                    artifact(sourcesJar)
                    artifact(dokkaJar)
                }

                pom {
                    packaging = "jar"
                    name(project.name)
                    description(project.description)
                    url("https://gitlab.com/CMDR_Tvis/${project.name}")
                    inceptionYear("2019")

                    developers {
                        developer {
                            email("postovalovya@gmail.com")
                            id("CMDR_Tvis")
                            name("Commander Tvis")
                            roles("architect", "developer")
                            timezone("7")
                            url("https://gitlab.com/CMDR_Tvis")
                        }
                    }

                    licenses {
                        license {
                            comments("Open-source license")
                            distribution("repo")
                            name("MIT License")
                            url("https://gitlab.com/CMDR_Tvis/${project.name}/blob/master/LICENSE")
                        }
                    }

                    scm {
                        connection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        developerConnection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        url("git@gitlab.com:CMDR_Tvis/${project.name}.git")
                    }
                }
            }

            repositories.boredApiGitlab {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }

                authentication { register(name = "header", type = HttpHeaderAuthentication::class) }
            }
        }
    }
}
