rootProject.name = "bored-api"
val kotlinVersion: String by settings

pluginManagement {
    plugins {
        val dokkaVersion: String by settings
        val kotlinVersion: String by settings
        id("org.jetbrains.dokka") version dokkaVersion
        kotlin(module = "jvm") version kotlinVersion
    }
}
